<?php
/**
 * @file
 * cron_roles.rules.inc
 * Adds a Rules action for pushing and popping roles
 * during cron executions
 * @author ctrahey@metaltoad.com
 */

/**
 * implements hook_rules_action_info()
 * defines push and pop actions for temporary roles
 */
function cron_roles_rules_action_info() {
  $defaults = array(
    'group' => t('Cron'),
  );
  $items['temp_add_roles'] = $defaults + array(
    'label' => t('Add Roles Temporarily'),
    'base' => 'cron_roles_temporarily_add_roles',
    'parameter' => array(
      'roles' => array(
        'type' => 'list<integer>',
        'label' => t('Temporary Roles'),
        'options list' => 'cron_roles_available_options',
        'description' => t('Choose the roles to apply from this point until the balanced use of "Revoke Temporary Roles". For global role additions, see <a href="@cron">the cron config page</a>.', array('@cron' => '/admin/config/system/cron')),
      ),
    ),
  );
  $items['pop_temp_roles'] = $defaults + array(
    'label' => t('Revoke Temporary Roles'),
    'base' => 'cron_roles_pop_temporary_roles',
  );
  return $items;
}

/**
 * Provides options to the rules UI when configuring roles to add
 */
function cron_roles_available_options() {
  $avail = user_roles(TRUE);
  return $avail;
}

/**
 * A stack of previous states of $GLOBAL['user']->roles
 * @return array (by reference) array of arrays, a snapshot of $user->roles
 */
function &_cron_roles_static_store() {
  static $roles = array();
  return $roles;
}

/**
 * If we are in the context of cron, this adds passed-in roles
 * to the current global $user, keeping a record of what roles
 * were previously configured in order to pop back later.
 * Users should balance calls with cron_roles_pop_temporary_roles()
 * @param array roles to add, as configured in the Rules UI
 * @see cron_roles_pop_temporary_roles()
 */
function cron_roles_temporarily_add_roles($roles) {
  if (!_cron_roles_run_cron_is_in_stack()) {
    return;
  }
  global $user;
  $available_roles = user_roles(TRUE);
  $chosen_roles = array_intersect_key($available_roles, $roles);
  $stack = &_cron_roles_static_store();
  array_push($stack, $user->roles);
  $user->roles += $chosen_roles;
  return;
}

/**
 * Reverts global $user's roles to the previous state
 * @see cron_roles_temporarily_add_roles()
 */
function cron_roles_pop_temporary_roles() {
  if (!_cron_roles_run_cron_is_in_stack()) {
    return;
  }
  global $user;
  $stack = &_cron_roles_static_store();
  $old_roles = array_pop($stack);
  $user->roles = $old_roles;
  return;
}
